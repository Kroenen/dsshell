#!/bin/bash
#Nombre de produit total
totProduit=0
#Nb de ref
nbRef=0

#initialise le fichier vide pour effectuer un premier scan
localfile=""
lastefile=""

touch ./log/log_bash.log
while [ true ]; do
	today=`date +%Y%m%d`
	yest=$(($today-1))
	this_day=`date +'[%Y-%m-%d]'`

	find_today=`find ./ERP/EXPMAG/*$today*`
	find_yest=`find ./ERP/EXPMAG/*$yest*`

	#On cherche le fichier et on le copie sur l'autre serveur
	if [ $find_today ]; then
	  file=${find_today##/*/}
	  echo $this_day ": Fichier trouvé " $find_today >> ./log/log_bash.log

	  echo $this_day ": -- Nouveau fichier detecté --" >> ./log/log_bash.log
	  echo $this_day ": Chemin du fichier : " $find_today >> ./log/log_bash.log
	  localfile=$find_today

	elif [ $find_yest ]; then
	  file=${find_yest##/*/}
	  echo $this_day ": Fichier trouvé " $find_yest >> ./log/log_bash.log

	  echo $this_day ": -- Nouveau fichier detecté --" >> ./log/log_bash.log
	  echo $this_day ": Chemin du fichier : " $find_yest >> ./log/log_bash.log
	  localfile=$find_yest
	else
		localfile=""
	fi

	if [ "$localfile" != "$lastefile" ]; then
		$lastefile=$localfile
		while IFS=';' read DateComm NumMAg EanProd QteProd
		do
			totProduit=$(($totProduit+$QteProd))

			if [ $nbRef -eq 0 ]; then
				ref=($EanProd)
				qtRef=($QteProd)
				nbRef=$(($nbRef+1))
			fi

			for (( i = 0; i < $nbRef; i++ )); do
				if [ "${ref[$i]}" == "$EanProd" ]; then
					qtRef[$i]=$(($qtRef+$QteProd))
				elif [ $i -eq $(($nbRef-1)) ]; then
					ref[$nbRef]=$EanProd
					qtRef[$nbRef]=$QteProd
					nbRef=$(($nbRef+1))
				fi
			done
		done < $localfile

		#sauvegarde donnée de verif

		for (( i = 0; i < $nbRef; i++ )); do
			echo "Ref: "${ref[$i]} ";Qt: " ${qtRef[i]} >> ./ERP/EXPMAG/CONTROLE/$this_day.txt
		done
		echo "Total Produit:" $totProduit >> ./ERP/EXPMAG/CONTROLE/$this_day.txt
		#sendMail
		
		#Fait une sauvegarde du fichier source
		cp $localfile ./SAVE/

		#deplace le fichier ver /ERP/CDEMAG
		mv $localfile ./ERP/CDEMAG/

	fi
done